nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
    
}

process start_codon_count {
  input:
    path infile
  output:
    path "${infile}.startcount", emit: wordcount
  script:
    """
    tail - 1 ${infile} | grep -i -o ATG ${infile} >> grepresult || true
    cat grepresult | wc -l > ${infile}.startcount
    """
}

process stop_codon_count {
  input:
    path infile
  output:
    path "${infile}.stopcount", emit: wordcount
  script:
    """
    tail - 1 ${infile} | grep -i -o TAA ${infile} >> grepresult || true
    tail - 1 ${infile} | grep -i -o TAG ${infile} >> grepresult || true
    tail - 1 ${infile} | grep -i -o TGA ${infile} >> grepresult || true
    cat grepresult | wc -l > ${infile}.stopcount
    """
}

process calculate_start {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "startcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > startcount
    """
}

process calculate_stop {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "stopcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > stopcount
    """
}

workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }
  inchannel = channel.fromPath(params.infile)
  splitfiles = split_file(inchannel)
  start_count = start_codon_count(splitfiles.flatten())
  calculate_start(start_count.collect())
  stop_count = stop_codon_count(splitfiles.flatten())
  calculate_stop(stop_count.collect())
}
