nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}

process count_word {
  input:
    path infile
    val word
    val word2
    val word3
  output:
    path "${infile}.wordcount", emit: wordcount
  script:
    """
    tail - 1 ${infile} | grep -i -o ${word} ${infile} >> grepresult || true
    tail - 1 ${infile} | grep -i -o ${word2} ${infile} >> grepresult || true
    tail - 1 ${infile} | grep -i -o ${word3} ${infile} >> grepresult || true
    cat grepresult | wc -l > ${infile}.wordcount
    """
}

process calculate_total {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "wordcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > wordcount
    """
}

workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }
  inchannel = channel.fromPath(params.infile)
  splitfiles = split_file(inchannel)
  allcounts = count_word(splitfiles.flatten(), params.word, params.word2, params.word3)
  calculate_total(allcounts.collect())
}
