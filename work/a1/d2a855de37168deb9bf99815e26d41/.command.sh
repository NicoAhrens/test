#!/bin/bash -ue
tail - 1 sequences.fasta.line01 | grep -i -o TAG sequences.fasta.line01 > grepresult || true
tail - 1 sequences.fasta.line01 | grep -i -o TAA sequences.fasta.line01 > grepresult2 || true
tail - 1 sequences.fasta.line01 | grep -i -o TGA sequences.fasta.line01 > grepresult3 || true
cat grepresult | wc -l > sequences.fasta.line01.wordcount
cat grepresult2 | wc -l > sequences.fasta.line01.wordcount
cat grepresult3 | wc -l > sequences.fasta.line01.wordcount
