#!/bin/bash -ue
tail - 1 sequences.fasta.line03 | grep -i -o TAG sequences.fasta.line03 >> grepresult || true
tail - 1 sequences.fasta.line03 | grep -i -o TAA sequences.fasta.line03 >> grepresult || true
tail - 1 sequences.fasta.line03 | grep -i -o TGA sequences.fasta.line03 >> grepresult || true
cat grepresult | wc -l >> sequences.fasta.line03.wordcount
