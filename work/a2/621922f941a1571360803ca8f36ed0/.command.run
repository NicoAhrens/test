#!/bin/bash
# NEXTFLOW TASK: calculate_total
set -e
set -u
NXF_DEBUG=${NXF_DEBUG:=0}; [[ $NXF_DEBUG > 1 ]] && set -x
NXF_ENTRY=${1:-nxf_main}


nxf_sleep() {
  sleep $1 2>/dev/null || sleep 1;
}

nxf_date() {
    local ts=$(date +%s%3N);
    if [[ ${#ts} == 10 ]]; then echo ${ts}000
    elif [[ $ts == *%3N ]]; then echo ${ts/\%3N/000}
    elif [[ $ts == *3N ]]; then echo ${ts/3N/000}
    elif [[ ${#ts} == 13 ]]; then echo $ts
    else echo "Unexpected timestamp value: $ts"; exit 1
    fi
}

nxf_env() {
    echo '============= task environment ============='
    env | sort | sed "s/\(.*\)AWS\(.*\)=\(.\{6\}\).*/\1AWS\2=\3xxxxxxxxxxxxx/"
    echo '============= task output =================='
}

nxf_kill() {
    declare -a children
    while read P PP;do
        children[$PP]+=" $P"
    done < <(ps -e -o pid= -o ppid=)

    kill_all() {
        [[ $1 != $$ ]] && kill $1 2>/dev/null || true
        for i in ${children[$1]:=}; do kill_all $i; done
    }

    kill_all $1
}

nxf_mktemp() {
    local base=${1:-/tmp}
    if [[ $(uname) = Darwin ]]; then mktemp -d $base/nxf.XXXXXXXXXX
    else TMPDIR="$base" mktemp -d -t nxf.XXXXXXXXXX
    fi
}

on_exit() {
    exit_status=${nxf_main_ret:=$?}
    printf $exit_status > /home/nico/ngsmodule/my_nextflow/work/a2/621922f941a1571360803ca8f36ed0/.exitcode
    set +u
    [[ "$tee1" ]] && kill $tee1 2>/dev/null
    [[ "$tee2" ]] && kill $tee2 2>/dev/null
    [[ "$ctmp" ]] && rm -rf $ctmp || true
    exit $exit_status
}

on_term() {
    set +e
    [[ "$pid" ]] && nxf_kill $pid
}

nxf_launch() {
    /bin/bash -ue /home/nico/ngsmodule/my_nextflow/work/a2/621922f941a1571360803ca8f36ed0/.command.sh
}

nxf_stage() {
    true
    # stage input files
    rm -f sequences.fasta.line02.wordcount
    rm -f sequences.fasta.line01.wordcount
    rm -f sequences.fasta.line03.wordcount
    rm -f sequences.fasta.line00.wordcount
    ln -s /home/nico/ngsmodule/my_nextflow/work/be/8aa6d8ed425c55384d916794c0128d/sequences.fasta.line02.wordcount sequences.fasta.line02.wordcount
    ln -s /home/nico/ngsmodule/my_nextflow/work/ac/75dbf6c8dc40e26586cda217561196/sequences.fasta.line01.wordcount sequences.fasta.line01.wordcount
    ln -s /home/nico/ngsmodule/my_nextflow/work/63/3ae1c27461a5a47079aad6a79e2405/sequences.fasta.line03.wordcount sequences.fasta.line03.wordcount
    ln -s /home/nico/ngsmodule/my_nextflow/work/ed/baed24b1bb94c291e68628ce452113/sequences.fasta.line00.wordcount sequences.fasta.line00.wordcount
}

nxf_unstage() {
    true
    [[ ${nxf_main_ret:=0} != 0 ]] && return
}

nxf_main() {
    trap on_exit EXIT
    trap on_term TERM INT USR2
    trap '' USR1

    [[ "${NXF_CHDIR:-}" ]] && cd "$NXF_CHDIR"
    NXF_SCRATCH=''
    [[ $NXF_DEBUG > 0 ]] && nxf_env
    touch /home/nico/ngsmodule/my_nextflow/work/a2/621922f941a1571360803ca8f36ed0/.command.begin
    set +u
    set -u
    [[ $NXF_SCRATCH ]] && echo "nxf-scratch-dir $HOSTNAME:$NXF_SCRATCH" && cd $NXF_SCRATCH
    nxf_stage

    set +e
    local ctmp=$(set +u; nxf_mktemp /dev/shm 2>/dev/null || nxf_mktemp $TMPDIR)
    local cout=$ctmp/.command.out; mkfifo $cout
    local cerr=$ctmp/.command.err; mkfifo $cerr
    tee .command.out < $cout &
    tee1=$!
    tee .command.err < $cerr >&2 &
    tee2=$!
    ( nxf_launch ) >$cout 2>$cerr &
    pid=$!
    wait $pid || nxf_main_ret=$?
    wait $tee1 $tee2
    nxf_unstage
}

$NXF_ENTRY
