#!/bin/bash -ue
tail - 1 sequences.fasta.line00 | grep -i -o TAG sequences.fasta.line00 >> grepresult || true
tail - 1 sequences.fasta.line00 | grep -i -o TAA sequences.fasta.line00 >> grepresult || true
tail - 1 sequences.fasta.line00 | grep -i -o TGA sequences.fasta.line00 >> grepresult || true
cat grepresult | wc -l > sequences.fasta.line00.wordcount
