#!/bin/bash -ue
tail - 1 sequences.fasta.line01 | grep -i -o TAG sequences.fasta.line01 >> grepresult || true
tail - 1 sequences.fasta.line01 | grep -i -o TAA sequences.fasta.line01 >> grepresult || true
tail - 1 sequences.fasta.line01 | grep -i -o TGA sequences.fasta.line01 >> grepresult || true
cat grepresult | wc -l >> sequences.fasta.line01.wordcount
