#!/bin/bash -ue
tail - 1 sequences.fasta.line00 | grep -i -o TAG sequences.fasta.line00 > grepresult || true
tail - 1 sequences.fasta.line00 | grep -i -o TAA sequences.fasta.line00 > grepresult2 || true
tail - 1 sequences.fasta.line00 | grep -i -o TGA sequences.fasta.line00 > grepresult3 || true
cat grepresult | wc -l > sequences.fasta.line00.wordcount
cat grepresult2 | wc -l > sequences.fasta.line00.wordcount
cat grepresult3 | wc -l > sequences.fasta.line00.wordcount
