#!/bin/bash -ue
tail - 1 sequences.fasta.line02 | grep -i -o TAG sequences.fasta.line02 >> grepresult || true
tail - 1 sequences.fasta.line02 | grep -i -o TAA sequences.fasta.line02 >> grepresult || true
tail - 1 sequences.fasta.line02 | grep -i -o TGA sequences.fasta.line02 >> grepresult || true
cat grepresult | wc -l > sequences.fasta.line02.wordcount
